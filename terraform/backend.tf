terraform {
  backend "s3" {
    bucket = "ci-bucket-cd"
    key    = "terraform.tfstate"
    region = "us-east-2"
  }

}