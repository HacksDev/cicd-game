# variable "access_key" {
#   type = string
# }

# variable "secret_key" {
#   type = string
# }



provider "aws" {
  # access_key = var.access_key
  # secret_key = var.secret_key
  region     = "us-east-2"
}


data "terraform_remote_state" "aws" {
  backend = "s3"
  config = {
    bucket = "ci-bucket-cd"
    key    = "terraform.tfstate"
    region = "us-east-2"
  }
}