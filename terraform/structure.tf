resource "aws_vpc" "production-vpc" {
  cidr_block  = "172.35.0.0/16"
}

resource "aws_subnet" "production-subnet" {
  vpc_id     = aws_vpc.production-vpc.id
  map_public_ip_on_launch = true
  cidr_block = "172.35.0.128/25"
}

resource "aws_internet_gateway" "production-internet-gw" {
  vpc_id = aws_vpc.production-vpc.id
}


resource "aws_route_table" "r-table" {
  vpc_id = aws_vpc.production-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.production-internet-gw.id
  }
}

resource "aws_main_route_table_association" "a" {
  vpc_id         = aws_vpc.production-vpc.id
  route_table_id = aws_route_table.r-table.id
}


resource "aws_security_group" "allow-web" {
  name        = "allow_web"
  description = "Allow nodejs web"
  vpc_id      = aws_vpc.production-vpc.id

  ingress {
    description = "NODEJS 3000"
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}


resource "aws_instance" "production" {
  ami = "ami-0f7919c33c90f5b58"
  subnet_id = aws_subnet.production-subnet.id
  security_groups = [aws_security_group.allow-web.id]
  user_data = <<DATA
#!/bin/bash
yum update
curl -sL https://rpm.nodesource.com/setup_10.x | sudo bash -
yum install -y nodejs git 
git clone https://gitlab.com/HacksDev/cicd-game.git
cd ./cicd-game
git checkout -b develop
npm install
npm run start
DATA
  instance_type = "t2.micro"
}

# Output section
output "instance_ip_addr" {
  value = aws_instance.production.public_ip
}
