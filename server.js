let express = require('express');
let app = express();
let defaultPrint = require("./modules/print") 

app.get('/', function (req, res) {
  res.send(defaultPrint());
});

let server = app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

app.get('/exit', function (req, res) {
    server.close();
});