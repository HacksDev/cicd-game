const defaultPrint = require('../modules/print');

test('Testing default response.', () => {
  expect(defaultPrint()).toBe("Hello World!");
});