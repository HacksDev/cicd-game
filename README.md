# cicd-game

## This is an simple web application based on Express (NodeJS)

1. The default route that may send to you ```"Hello World"``` was created.
2. The only one single test based on Jest was implemented
3. Here you can find the ```gitlab-ci.yml``` that contains the configuration file for the gitlab's pipline. 
Pipline contains the following stages:
```
  - build 
  - test
  - staging
  - deploy
```
4. This uses Terraform to automatically create infrastructure in the AWS cloud.

## How to start server locally

```
npm run start 
```

## How to run terraform

1. Choose the terraform folder and create there a ```terraform.tfvars``` file.
2. Put there your credentials: access and secret keys.
```
access_key = "..."
secret_key = "..."
```
3. Please, make sure that your user has minimal required privileges on AWS. The less it can use, the safer it is.
4. Run ```terraform init``` and then ```terraform apply -var-file=terraform.tfvars```.
5. Check that the instance was created and find its public IP address.
6. Try to connect and enjoy.